/**
 * class Comments javadoc
 * 
 * @author Adam Vorley
 * @version 1.0
 */
import java.util.Scanner; 

public class Comments {

    public static void main(String[] args) {

        // single comment
        // Output distance from (0, 0) to (10, 20)
        System.out.println(Math.sqrt(10 * 10 + 20 * 20));

        // multiline comment
        /**
         *    A year is a leap year when it's divisible by 400, or divisible by 4 and
         * not also divisible by 100
         */

        // Have the user input a year to test the rule
        
        Scanner userInput = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter a year");

        int year = userInput.nextInt();  // Read user input

        
       // int year = 2024;
        System.out.println(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0));

    }
}

